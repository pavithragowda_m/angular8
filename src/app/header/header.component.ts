import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  add(){
    document.getElementById("header-id").style.backgroundColor="yellow"
  }

  ngOnInit() {
    this.add()
  }

}
