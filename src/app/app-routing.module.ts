import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { DirectiveComponent } from './directive/directive.component';
import { BindingComponent } from './binding/binding.component';
import { ServiceDependencyInjectionComponent } from './service-dependency-injection/service-dependency-injection.component'
import { FrontendBackendCommnComponent } from './frontend-backend-commn/frontend-backend-commn.component'

// components added for the routing
const routes: Routes = [
  {path: 'header', component: HeaderComponent},
  {path: 'directive', component: DirectiveComponent},
  {path: 'binding', component:BindingComponent},
  {path: 'service', component:ServiceDependencyInjectionComponent},
  {path: 'backend', component:FrontendBackendCommnComponent},
  {path: '', redirectTo: '/header', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
