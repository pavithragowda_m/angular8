import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {

  constructor() { }
  myname:string="pavithra";
  myphonenum:number=123456789;
  myvalue = "two way data binding"
  // property binding
  myfunc(){
    const input = document.querySelector('input');
    console.log("attribute",input.getAttribute('value')); 
    console.log("value property",input.value); 
  }
  
  // dom event binding
  mouseoverfunc(){
    const ele= document.getElementById("p-id")
    ele.style.fontWeight="600"
  }
  mouseoutfunc(){
    const ele= document.getElementById("p-id")
    ele.style.fontWeight="100"
  }

  sample(input){
    console.log(input)
  }

  // two way data binding
  submit(myinputvalue:string){
    
    let myvalue = document.getElementById("two-way")
    myvalue.textContent=myinputvalue

  }
  // using ng model in two way data binding
  mydatavalue:string = "pavithra"

  ngOnInit() {
  }

}
