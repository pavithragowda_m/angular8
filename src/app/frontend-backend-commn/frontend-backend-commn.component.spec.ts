import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontendBackendCommnComponent } from './frontend-backend-commn.component';

describe('FrontendBackendCommnComponent', () => {
  let component: FrontendBackendCommnComponent;
  let fixture: ComponentFixture<FrontendBackendCommnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontendBackendCommnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontendBackendCommnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
