import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-frontend-backend-commn',
  templateUrl: './frontend-backend-commn.component.html',
  styleUrls: ['./frontend-backend-commn.component.css']
})
export class FrontendBackendCommnComponent implements OnInit {

  // --------------------reference--------------------------
  // https://jasonwatmore.com/post/2019/09/06/angular-http-get-request-examples
  // https://www.djamware.com/post/5d8d7fc10daa6c77eed3b2f2/angular-8-tutorial-rest-api-and-httpclient-examples
  // https://angular.io/guide/http  -----http in service file

  get_data: any = null
  post_data: any = null
  myjson = JSON


  constructor(private http: HttpClient) { 
    setTimeout(() => {
      this.http.get<any>('https://reqres.in/api/users?page=4').subscribe(data => {
        this.get_data = data;


        setTimeout(() => {
          this.http.post<any>('https://reqres.in/api/users', {
            "name": "morpheus",
            "job": "leader"
          }).subscribe(data => {
            this.post_data = data;
          })
        }, 2000);

      })
    }, 4000);

  }


  ngOnInit() {
  }

}
