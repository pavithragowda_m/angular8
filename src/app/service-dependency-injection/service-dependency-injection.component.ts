import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service'    // we need to import the service created in order to use service

@Component({
  selector: 'app-service-dependency-injection',
  templateUrl: './service-dependency-injection.component.html',
  styleUrls: ['./service-dependency-injection.component.css']
})
export class ServiceDependencyInjectionComponent implements OnInit {

  data: any
  

  constructor(service:DataService) {    //we need to inject the service created in order to access it syntax:- varName: serviceName
    this.data=service.getData()        // in order to access the method written in service we need to mention the method name syntax:- varName.methodName
   }

  ngOnInit() {
  }

}
