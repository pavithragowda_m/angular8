import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceDependencyInjectionComponent } from './service-dependency-injection.component';

describe('ServiceDependencyInjectionComponent', () => {
  let component: ServiceDependencyInjectionComponent;
  let fixture: ComponentFixture<ServiceDependencyInjectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceDependencyInjectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceDependencyInjectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
