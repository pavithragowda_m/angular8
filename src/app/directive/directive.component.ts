import { Component, OnInit } from '@angular/core';
import {map} from 'rxjs/operators'
@Component({
  selector: 'app-directive',
  templateUrl: './directive.component.html',
  styleUrls: ['./directive.component.css']
})
 
export class DirectiveComponent implements OnInit {
  
  show: boolean = true; 
  data={"name":"terralogic","phno":123456789}
  mydata=[
    {
      "company":"terralogic",
      "company_id":123,
      "details":[{
        "no_of_engineers":50,
        "no_of_managers":5
      }]
    },
    {
      "company":"lollypop",
      "company_id":456,
      "details":[{
        "no_of_engineers":25,
        "no_of_managers":3
      }]
    }
  ]

  myFunc(a){
    console.log(this.data)
  }
  arrowFunc=(b) => {
    alert("arrow func is clicked"+b)
  }
  
  // on load of application 
  ngOnInit(){
    this.myFunc(15)
  }
    
  


}
