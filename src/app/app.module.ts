import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { DirectiveComponent } from './directive/directive.component';
import { BindingComponent } from './binding/binding.component';
import { FormsModule } from '@angular/forms';
import { ServiceDependencyInjectionComponent } from './service-dependency-injection/service-dependency-injection.component'
import { DataService } from './data.service';
import { HttpClientModule } from '@angular/common/http';
import { FrontendBackendCommnComponent } from './frontend-backend-commn/frontend-backend-commn.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DirectiveComponent,
    BindingComponent,
    ServiceDependencyInjectionComponent,
    FrontendBackendCommnComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [DataService],   //we need to register the custom service created
  bootstrap: [AppComponent]
})
export class AppModule { }
